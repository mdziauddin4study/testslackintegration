﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Diagnostics;
using System.Text;

namespace SASTIntegration
{
    class CommandInjection
    {
        public void StartProcess(string input)
        {
            var p = new Process();
            p.StartInfo.FileName = "exportLegacy.exe";
            p.StartInfo.Arguments = " -user " + input + " -role user";
            p.Start();
        }
    }
}
