﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Security.Cryptography;
using System.Text;

namespace RSA.Archer.Api.Client
{
    class CipherBug
    {
        public byte[] Method()
        {
            byte[] cipherText;
            using (var aes = new AesManaged
            {
                KeySize = 10,
                BlockSize = 10,
                Mode = CipherMode.OFB,
                Padding = PaddingMode.PKCS7
            })
            {
                using (var encrypter = aes.CreateEncryptor(new byte[10], new byte[16]))
                using (var cipherStream = new MemoryStream())
                {
                    using (var cryptoStream = new CryptoStream(cipherStream, encrypter, CryptoStreamMode.Write))
                    using (var binaryWriter = new BinaryWriter(cryptoStream))
                    {
                        //Encrypt Data
                        binaryWriter.Write("secret message");
                    }
                    cipherText = cipherStream.ToArray();
                }
            }

            return cipherText;
        }
    }
}
